<?php
	/*
	# mod_sp_tweet - Twitter Module by JoomShaper.com
	# -----------------------------------------------
	# Author    JoomShaper http://www.joomshaper.com
	# Copyright (C) 2010 - 2014 JoomShaper.com. All Rights Reserved.
	# license - GNU/GPL V2 or Later
	# Websites: http://www.joomshaper.com
	*/

	// no direct access
	defined('_JEXEC') or die('Restricted access');

	require_once JPATH_COMPONENT . '/helpers/TwitterAPIExchange.php';
	
	jimport('joomla.filesystem.file');

	class ContentHelperTwitter{
		private $cacheParams=array();
		private $api;
		
		//Initiate configurations
		public function __construct($options = array()) {
			
			parent::__construct($options);
		}
		
		/*
		* get twitter datas
		*/		
		public function getData()
		{
			$userName = 'Astroboysoup1';
			$numTweets = 1;

			$settings = array(
				'consumer_key' => 'RmLV87ZHy4mtRfr3dhXYa2ilF',
				'consumer_secret' => '9W22Orkm1ttrDb6EITu2vVVmVj256SpcSublSsYxYUhtoaDJ31',
				'oauth_access_token' => '709440370128478208-rmFrggVg6LykkH7lihgpgDepWAUpgNq',
				'oauth_access_token_secret' => 'MWi6MZAnHcBXw5ZNKw74yTJ3rmcJ4ulnqXO8a7UIOZK96'
				);

			if( empty($settings['consumer_key']) ){
				JError::raiseNotice( 100, 'Twitter Consumer Key not defined.' );
				return NULL;
			} 			
			elseif( empty($settings['consumer_secret']) ){
				JError::raiseNotice( 100, 'Twitter Consumer secret not defined.' );
				return NULL;
			} 
			elseif( empty($settings['oauth_access_token']) ){
				JError::raiseNotice( 100, 'Twitter access token not defined.' );
				return NULL;
			} 
			elseif( empty($settings['oauth_access_token_secret']) ){
				JError::raiseNotice( 100, 'Twitter access token secret not defined.' );
				return NULL;
			} 



			$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
			$getfield = '?include_entities=true&include_rts=true&screen_name='.$userName.'&count='. $numTweets;
			$requestMethod = 'GET';
			$this->api = new TwitterAPIExchange($settings);
			return $this->api->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();

		}
		
		/*
		* Function to get tweets
		*/			
		public function tweets() { 
				$data = ContentHelperTwitter::getData();

			return json_decode($data, true);
		}

		/*
		* Prepare feeds
		*/			
		public function prepareTweet($string) {
			//Url
			$pattern = '/((ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?)/i';
			$replacement = '<a target="_blank" class="tweet_url" href="$1">$1</a>';
			$string = preg_replace($pattern, $replacement, $string);

			
				$pattern = '/[\#]+([A-Za-z0-9-_]+)/i';
				$replacement = ' <a target="_blank" class="tweet_search" href="http://search.twitter.com/search?q=$1">#$1</a>';
				$string = preg_replace($pattern, $replacement, $string);

			
				$pattern = '/\s[\@]+([A-Za-z0-9-_]+)/i';
				$replacement = ' <a target="_blank" class="tweet_mention" href="http://twitter.com/$1">@$1</a>';
				$string = preg_replace($pattern, $replacement, $string);	
			

			
				$pattern = '/\s([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})/i';
				$replacement = ' <a target="_blank" class="tweet_email" href="mailto:$1">$1</a>';
				$string = preg_replace($pattern, $replacement, $string);
			
			return $string;
		}

		//Function for converting time
		public function timeago($timestamp) {
			$time_arr 		= explode(" ",$timestamp);
			$year 			= $time_arr[5];
			$day 			= $time_arr[2];
			$time 			= $time_arr[3];
			$time_array 	= explode(":",$time);
			$month_name 	= $time_arr[1];
			$month = array (
				'Jan' => 1,
				'Feb' => 2,
				'Mar' => 3,
				'Apr' => 4,
				'May' => 5,
				'Jun' => 6,
				'Jul' => 7,
				'Aug' => 8,
				'Sep' => 9,
				'Oct' => 10,
				'Nov' => 11,
				'Dec' => 12
			);

			$delta = gmmktime(0, 0, 0, 0, 0) - mktime(0, 0, 0, 0, 0);
			$timestamp = mktime($time_array[0], $time_array[1], $time_array[2], $month[$month_name], $day, $year);
			$etime = time() - ($timestamp + $delta);
			if ($etime < 1) {
				return '0 seconds';
			}

			$a = array( 12 * 30 * 24 * 60 * 60  =>  'YEAR',
				30 * 24 * 60 * 60       =>  'MONTH',
				24 * 60 * 60            =>  'DAY',
				60 * 60                 =>  'HOUR',
				60                      =>  'MINUTE',
				1                       =>  'SECOND'
			);

			foreach ($a as $secs => $str) {
				$d = $etime / $secs;
				if ($d >= 1) {
					$r = round($d);
					return $r . ' ' . JText::_($str . ($r > 1 ? 'S' : ''));
				}
			}
		}
	}