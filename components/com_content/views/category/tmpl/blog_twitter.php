<?php
    // no direct access
    defined( '_JEXEC' ) or die( 'Restricted access' );
    $data = ContentHelperTwitter::tweets();
?>

<div class="tweet-feed">
    <?php foreach($data as $i=>$value) {
	
	?>
        <div class="cat-tweet-item">		
		        <a href="http://twitter.com/<?php echo $value['user']['screen_name']; ?>" target="_blank">
                        <img width="48" height="48" alt="<?php echo $value['user']['name']; ?>" title="<?php echo $value['user']['name']; ?>" src="<?php echo $value['user']['profile_image_url']; ?>" class="tweet-avatar">
                    </a>
                <div class="date"><a target="_blank" href="http://twitter.com/<?php echo $value['user']['screen_name']; ?>/status/<?php 
						echo  $value['id_str']; ?>"><?php echo  JText::_('ABOUT') . '&nbsp;' . ContentHelperTwitter::timeago( $value['created_at'] ) . '&nbsp;' . JText::_('AGO'); ?> </a></div>	
							
						
									<div class="source"><?php echo JText::_('FROM') . ' ' . $value['source']; ?></a></div>
														<br>
								<?php echo ContentHelperTwitter::prepareTweet($value['text']); ?>
		<div class="tweet-clr"></div>
        </div>
    <?php } ?>
</div>
<div class="tweet-clr"></div> 
    <a class="followme" target="_blank" href="http://twitter.com/<?php echo $data[0]['user']['screen_name'] ?>"><?php echo JText::_('FOLLOW') . ' ' . $data[0]['user']['name'] . ' ' . JText::_('ON_TWITTER') ?></a>
<div class="tweet-clr"></div>